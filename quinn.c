#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct particle {
   long ID;
   double x;
   double y;
   double z;
   double vx;
   double vy;
   double vz;   
};

double ax(double x, double y, double z, double GM_over_a2_plummer) //conservative force (well, acceleration of the plummer)
{
	double r2 = x*x + y*y + z*z; //x, y, z, are expressed in units of a_plummer
	double coefficient = -GM_over_a2_plummer*(1.0/pow(1.0+r2,1.5));
    return(coefficient*x);
}

double ay(double x, double y, double z, double GM_over_a2_plummer) //conservative force (well, acceleration of the plummer)
{
	double r2 = x*x + y*y + z*z; //x, y, z, are expressed in units of a_plummer
	double coefficient = -GM_over_a2_plummer*(1.0/pow(1.0+r2,1.5));
    return(coefficient*y);
}

double az(double x, double y, double z, double GM_over_a2_plummer) //conservative force (well, acceleration of the plummer)
{
	double r2 = x*x + y*y + z*z; //x, y, z, are expressed in units of a_plummer
	double coefficient = -GM_over_a2_plummer*(1.0/pow(1.0+r2,1.5));
    return(coefficient*z);
}

struct particle onestep(struct particle p, double GM_over_a2_plummer, double dt, double Omega)
{
    double a_n_x = ax(p.x, p.y, p.z, GM_over_a2_plummer);
    double a_n_y = ay(p.x, p.y, p.z, GM_over_a2_plummer);
    double a_n_z = az(p.x, p.y, p.z, GM_over_a2_plummer);	
    double vx_n_14 = p.vx - 0.5*dt*(Omega*Omega*p.x - a_n_x);
    double P_n_y = p.vy + 2.0*Omega*p.x + 0.5*dt*a_n_y;
    double vx_n_12 = vx_n_14 + dt*Omega*P_n_y;
    double vy_n_12 = P_n_y - Omega*p.x - Omega*(p.x + dt*vx_n_12);
    double vz_n_12 = p.vz + 0.5*dt*(-Omega*Omega*p.z + a_n_z);

    p.x = p.x + dt*vx_n_12;
    p.y = p.y + dt*vy_n_12;
    p.z = p.z + dt*vz_n_12;

    double a_n1_x = ax(p.x, p.y, p.z, GM_over_a2_plummer);
    double a_n1_y = ay(p.x, p.y, p.z, GM_over_a2_plummer);
    double a_n1_z = az(p.x, p.y, p.z, GM_over_a2_plummer);
    double vx_n_34 = vx_n_12 + dt*Omega*P_n_y;
    double vx_n_1 = vx_n_34 - 0.5*dt*(Omega*Omega*p.x - a_n1_x);
    double vy_n_1 = P_n_y - 2.0*Omega*p.x + 0.5*dt*a_n1_y;
    double vz_n_1 = vz_n_12 + 0.5*dt*(-Omega*Omega*p.z + a_n1_z);

    p.vx = vx_n_1;
    p.vy = vy_n_1;
    p.vz = vz_n_1;

    return(p);
}

struct particle manysteps(struct particle p, double GM_over_a2_plummer, double dt, double Omega, long N)
{
    long i;
    for(i=0; i<N; i++)
    {
    	p = onestep(p, GM_over_a2_plummer, dt, Omega);
    }
    return(p);
}

double normal() //normal random numbers with box muller transform
{
    double U1 = (double)rand()/(double)(RAND_MAX);
    double U2 = (double)rand()/(double)(RAND_MAX);
    double Z = sqrt(-2.0*log(U1))*cos(2*M_PI*U2);
    return(Z);
}

struct particle generate_particle(long i, double velocity_scale)
{
	struct particle p;
	double s = sqrt(3.0);
	p.ID = i;

    p.x = normal()/s;
    p.y = normal()/s;
    p.z = normal()/s;
    p.vx = velocity_scale*normal()/s;
    p.vy = velocity_scale*normal()/s;
    p.vz = velocity_scale*normal()/s;
    return(p);
}

int main()
{
	long number_of_particles = 50000;
	double radius_of_popII = 0.8;
	double distance_at_which_a_particle_is_considered_an_escaper = 100.0; //in units of the Plummer radius, a
	long evolve_for_how_many_crossing_times = 1000.0;
	double GM_over_a2_plummer = 1.0; //GM/a^2 of the Plummer model
    double Omega = 0.1;
    double M_gal_m_GC_ratio = 100000;
    double rapporto = Omega*Omega / GM_over_a2_plummer;
    printf("Centrifugal to gravitational force ratio at scale radius: %f \n", rapporto);
    double crossing_time = 1.0 / sqrt(GM_over_a2_plummer);
    printf("Crossing time: %f \n", crossing_time);
    double revolution = 2.0 * M_PI / Omega;
    printf("Revolution time: %f \n", revolution);

    double D_gal_GC_in_units_of_Plummer_radius = pow(M_gal_m_GC_ratio, 1.0/3.0)*pow(Omega*crossing_time, -2.0/3.0);

    double dt = 0.1;

    //velocity scale is GM/a^2 * crossing_time... 
    double velocity_scale = GM_over_a2_plummer * crossing_time; //it is the circular velocity at a

    double safety_factor = 0.1; //dt should not exceed a fraction (set by safety_factor) of the two timescales
    if((dt > safety_factor*revolution) || (dt > safety_factor*crossing_time))
    {
    	printf("The integrator stepsize is too big\n");
    } 

    //printf("%f %f %f", ax(1,0,0, GM_over_a2_plummer), ay(0,1,0, GM_over_a2_plummer), az(0,0,1,GM_over_a2_plummer));
    struct particle final_p, p;
    double r2;
    double initial_r2;
    long i;

    srand(37);
    long escapers = 0;
    long popIIescapers = 0;
    long popIIparticles = 0;

    for(i = 0; i < number_of_particles; i++)
    {
        p = generate_particle(i, velocity_scale);
        final_p = manysteps(p, GM_over_a2_plummer, dt, Omega, evolve_for_how_many_crossing_times*(long)(crossing_time/dt));
        initial_r2 = (p.x*p.x + p.y*p.y + p.z*p.z);
        if(initial_r2 < radius_of_popII*radius_of_popII)
        {
            popIIparticles++;
        }
        r2 = (final_p.x*final_p.x + final_p.y*final_p.y + final_p.z*final_p.z);
        if(r2 > distance_at_which_a_particle_is_considered_an_escaper*distance_at_which_a_particle_is_considered_an_escaper)
        { //print only the escapers (ID, x, y, z, vx, vy, vz at the start, x, y, z, vx, vy, vz at the end)
            escapers++;
            if(initial_r2 < radius_of_popII*radius_of_popII)
            {
                popIIescapers++;
            }
            //printf("%lu %f %f %f %f %f %f %f %f %f %f %f %f\n", final_p.ID, p.x, p.y, p.z, p.vx, p.vy, p.vz, final_p.x, final_p.y, final_p.z, final_p.vx, final_p.vy, final_p.vz);
        } //you can use the initial x, y, z to find which escapers were pop II (they had radius within some cutoff radius)
    }
    printf("Particles: %lu, PopII particles: %lu, Escapers: %lu, PopII escapers: %lu\n", number_of_particles, popIIparticles, escapers, popIIescapers);
    printf("\n");
    printf("%f %f\n", D_gal_GC_in_units_of_Plummer_radius, ((double)popIIescapers)/((double)escapers));
	return(0);
}
